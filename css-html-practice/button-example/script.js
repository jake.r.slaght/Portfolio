messageButton = document.querySelector("button.message");
aboutButton = document.querySelector("button.about");
locationButton = document.querySelector("button.location");
settingsButton = document.querySelector("button.settings");
container = document.querySelector(".button-container");

messageButton.addEventListener("mouseover", () => setColorProperty(messageButton));
messageButton.addEventListener("mouseout",() => resetColorProperty(messageButton));

aboutButton.addEventListener("mouseover", () => setColorProperty(aboutButton));
aboutButton.addEventListener("mouseout", () => resetColorProperty(aboutButton));

locationButton.addEventListener("mouseover", () => setColorProperty(locationButton));
locationButton.addEventListener("mouseout", () => resetColorProperty(locationButton));

settingsButton.addEventListener("mouseover", () => setColorProperty(settingsButton));
settingsButton.addEventListener("mouseout", () => resetColorProperty(settingsButton));

function setColorProperty(button) {
    document.documentElement.style.setProperty('--color', button.dataset.hoverColor);
}

function resetColorProperty(button) {
    document.documentElement.style.setProperty('--color', button.dataset.baseColor);
}