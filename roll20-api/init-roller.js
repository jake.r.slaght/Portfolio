const tcpInit = (() => {
    var turnTracker = [];
    var numberOfActiveTokens = 0;
    var trackers = [];
    var currentTracker = [];
    var currentRound = 1;
    var trackRounds = false;
    var tokens;
    
    var prepareRolls = () => {
        var players = findObjs({ type: "character" });
        tokens = findObjs({ pageid: Campaign().get("playerpageid") });
        for (token of tokens) {
            if (token.get("layer") === "objects") {
                log("prepare rolls")
                var player = players.find(p => p.get("name") === token.get("name"));
                numberOfActiveTokens++;
                var playerDexMod = findObjs({ name: "initiative_bonus", _characterid: player.id })[0].get("current");
                var playerName = player.get("name");
                var id = token.id;
                rollInitative(playerName, playerDexMod, id, token.get("pageid"))
            }
        }
        Campaign().set("initiativepage", true);
    };

    var rollInitative = (playerName, playerDexMod, id, _pageid) => {
        sendChat("", "/roll 1d20+" + playerDexMod,
            function (ops) {
                var pr = JSON.parse(ops[0].content).total
                sendChat(`ARI - ${playerName}`, `rolled a ${pr} for initiative`);
                turnTracker.push({ id, pr, _pageid, custom: "" });
                if (numberOfActiveTokens === turnTracker.length) {
                    sortedTurns = _.sortBy(turnTracker, "pr").reverse();
                    Campaign().set("turnorder", JSON.stringify(sortedTurns));
                    currentTracker = JSON.parse(Campaign().get("turnorder"));
                }
            });
    };

    var saveCurrentOrder = (key) => {
        let existingTracker = trackers.find(tracker => tracker.key === key);
        if (existingTracker === undefined) {
            trackers.push({key, tracker: turnTracker});
            sendChat("ARI",`/w gm Saved current Initiative as <strong>${key}</strong>. Remember, saved initiatives only last for current session.`);
        } else if( existingTracker !== undefined) {
          sendChat("ARI", "/w gm A saved Tracker exists with that name, please try saving with a new name. Rewrite option is TBD");
        } else if( turnTracker.length === 0) {
          sendChat("ARI", "/w gm You did not roll for an initiative yet, please roll first. Option to roll with saving if not yet rolled TBD");
        }
    };
    
    var loadTurnOrder = (key) => {
      if(trackers.length === 0) {
          sendChat("ARI", "/w gm There is no trackers saved for this session to load.")
      } else {
          let tracker = trackers.find(tracker =>tracker.key === key);
          if (tracker === undefined) {
            sendChat("ARI", "/w gm There is no trackers saved by that name for this session. Please try a different name.")
          } else {
            sortedTurns = _.sortBy(tracker.tracker, "pr").reverse();
            Campaign().set("turnorder", JSON.stringify(sortedTurns));
            currentTracker = JSON.parse(Campaign().get("turnorder"));
            sendChat("ARI", `/w gm Saved Tracker <strong>${tracker.key}</strong> has been loaded`);
          }
      }
    };

    var selectSaveTurnOrderToLoad = () => {
        var links = '';
        if (trackers.length === 0) {
           sendChat("ARI", "/w gm no trackers saved."); 
        } else {
            _.each(trackers, (tracker) => {
               links+=`<a href="!tcp-init select-saved-order ${tracker.key}">${tracker.key}</a>`; 
            });
            
            sendChat("ARI", `/w gm Select the Saved Tracker you want to load: <br /> ${links}`);
        }
    };
    
    var generateTurnCard = (token, currentRound) => {
        let card = `<table style="border: 1px solid black; padding: 1rem; width: 100%">`;
        card +=`<thead style="background-color: indigo; color: navajowhite"><tr><th>Current Round: ${currentRound}</th></tr></thead>`;
        card +=`<tbody><tr><td><strong>${token.get("name")}</strong>'s Turn</td></tr></tbody>`;
        card +="</table>";
        sendChat("ARI", card);
        
        var players = findObjs({ type: "character" });
        var player = players.find(p => p.get("name") === token.get("name"));
        sendChat("ARI", `!pingCharacter ${player.get("id")} ${token.get("id")} ${token.get("name")} true`)
    }
    
    on("chat:message", (msg) => {
        if (msg.type !== "api") {
            return;
        }

        if (msg.content.indexOf("!tcp-init") === 0) {
            var commandInput = msg.content.split(" ");
            if (commandInput.length === 1) {
                if(playerIsGM(msg.playerid)) {
                turnTracker = [];
                numberOfActiveTokens = 0;

                Campaign().set("initiativepage", false);
                prepareRolls();
                } else {
                    sendChat("ARI", `/w ${msg.who} you do not have permission to roll initiatives`);
                }
            } else {
                switch (commandInput[1]) {
                    case "help":
                        log("working on the help")
                        break;
                    case "save":
                        if (playerIsGM(msg.playerid)) {
                            if (commandInput[2] != null) {
                                saveCurrentOrder(commandInput[2])
                            } else {
                                sendChat("ARI", "/w gm save command requires a order name");
                            }
                        } else {
                            sendChat("ARI", `/w ${msg.who} you do not have sufficient permissions to save turn tracker`);
                        }
                        break;
                    case "current-order":
                        log(Campaign().get("turnorder"));
                        break;
                    case "load-init":
                        if (playerIsGM(msg.playerid)) {
                            if (commandInput[2] != null) {
                                loadTurnOrder(commandInput[2])
                            } else {
                                selectSaveTurnOrderToLoad();
                            }
                        } else {
                            sendChat("ARI", `/w ${msg.who} you do not have sufficient permissions to load a turn tracker`);
                        }
                        break;
                    case "select-saved-order":
                        loadTurnOrder(commandInput[2]);
                        break;
                    case "combat-tracker":
                        if (playerIsGM(msg.playerid)) {
                            if (commandInput[2] === undefined) {
                                sendChat("ARI", "/w gm Invalid option. Y/N are only valid options");
                            } else if (commandInput[2].toLowerCase() === 'y') {
                                trackRounds = true;
                                if (currentTracker.length > 0) {
                                    sendChat("", `<h3 style="color: crimson">Combat has started.</h3>`);
                                    var first = currentTracker[0].id;
                                    var token = tokens.find(t => t.get("id") === first);
                                    generateTurnCard(token, currentRound);
                                }
                            } else if (commandInput[2].toLowerCase() === 'n') {
                                trackRounds = false;
                                currentRound = 1;
                                
                                sendChat("", `<h3 style="color: crimson">Combat has ended.</h3>`);
                            } else {
                                sendChat("ARI", "/w gm Invalid option. Y/N are only valid options");
                            }
                        } else {
                            log(msg)
                            sendChat("ARI", `/w ${msg.who} you do not have sufficient permissions to start or end combat`);
                        }
                        break;
                    default:
                        log("invalid command");
                        break;
                }
            }
        }
    });
    
    on("change:campaign:turnorder", (current, previous) => {
        var currentTO = JSON.parse(current.get("turnorder"));
        tokens = findObjs({ pageid: Campaign().get("playerpageid") });
        log("I changed turns")
        var token = tokens.find(t => t.get("id") === currentTO[0].id);

        if (trackRounds) {
            generateTurnCard(token, currentRound);

            if (currentTracker.length > 0) {
                var previousTO = JSON.parse(previous.turnorder)
                var first = currentTracker[0].id;
                var last = currentTracker[currentTracker.length - 1].id
                
                if(last === currentTO[0].id) {
                    currentRound += 1;
                }
            }
        }
    });
})();