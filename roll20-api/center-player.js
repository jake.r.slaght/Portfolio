const CenterPlayer = (() => {
    on("chat:message", (msg) => {
        if(msg.type === 'api' && msg.content.indexOf("!center-me") === 0) {
            var characterList = findObjs({type: "character", controlledby: msg.playerid});
            var playerName = getObj("player", msg.playerid).get("displayname");
            
            if(characterList.length === 1) {
                sendChat("Cemter Me", `!pingCharacter ${characterList[0].get("id")} ${msg.playerid} ${playerName}`);
            } else {
                var links = "";
                _.each(characterList, (character) => {
                    links += `<a href="!pingCharacter ${character.get('id')} ${msg.playerid} ${playerName}">${character.get("name")}</a>`;                
                });
                
                sendChat("Center Me", `/w ${playerName} Which character? <br/> ${links}`);
            }
        }
    });
    
    on("chat:message", (msg) => {
        if(msg.type==="api" && msg.content.indexOf("!pingCharacter") === 0) {
            var args = msg.content.split(" ");
            var characterID = args[1];
            var playerID = args[2];
            var playerName = args[3];
            
            log(args)
            var combat = args[4];
            
            var playerToken = findObjs({
                type: "graphic", 
                subtype: "token", 
                represents: characterID, 
                pageid: Campaign().get("playerpageid")
            })[0];
            
            if(playerToken === undefined){
                sendChat("Center Me", `/w ${playerName} That character is not on the map.`);
                return;
            }
            
            if(combat) {
                sendPing(playerToken.get("left"), playerToken.get("top"), playerToken.get("pageid"), null, true);
            } else {
                sendPing(playerToken.get("left"), playerToken.get("top"), playerToken.get("pageid"), playerID, true, playerID);
            }
        }
    });
})();