﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AppointmentTracker.Startup))]
namespace AppointmentTracker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
